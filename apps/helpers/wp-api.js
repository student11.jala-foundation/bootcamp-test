const NAME = "api.deezer",
DOMAIN =`https://cors-anywhere.herokuapp.com/https://${NAME}.com`,
LIST_SONGS = `${DOMAIN}/chart/0/tracks`,
SONGS_ID = `${SONG}/:id`,
ALBUM_ID = `${DOMAIN}/album/:id`,
SEARCH = `${DOMAIN}/search?q=eminem`;

export default{
    NAME,
    DOMAIN,
    LIST_SONGS,
    SONGS_ID,
    ALBUM_ID,
    SEARCH
}


